#include "Food.h"

using namespace yam2d;

Meal::Meal(Food* food, vec2 position)
	: AnimatedSpriteGameObject(0, food->sprite)
{
	this->setSize(vec2(32));
	this->setPosition(position);
	this->setName(food->name);
	direction = rand()%1000 - 500;

	this->food = food;

	std::vector<int> indices;
	indices.resize(4);
	for( size_t j=0; j<indices.size(); ++j )
	{
		indices[j] = j;
	}

	this->addAnimation(0, SpriteAnimation::SpriteAnimationClip(indices, 1, 1.0f, true));
	this->setActiveAnimation(0);
}


Meal::~Meal()
{
}

float Meal::getCalories()
{
	return food->calories;
}