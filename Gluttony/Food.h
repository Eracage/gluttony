#ifndef FOOD_H
#define FOOD_H

#include <AnimatedSpriteGameObject.h>

struct Food
{
	std::string name;
	float calories;
	yam2d::SpriteSheet* sprite;
};


class Meal : public yam2d::AnimatedSpriteGameObject
{
public:
	Meal(Food* food, yam2d::vec2 position);
	~Meal();

	float getCalories();

	std::string SpawnName;
	float direction;
	Food* food;

private:
	Meal();
	Meal(const Meal& meal);
	Meal& operator =(const Meal& meal);
};

#endif