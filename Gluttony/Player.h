#ifndef PLAYER_H
#define PLAYER_H

#include "HitCheck.h"
#include "Pathfinding.h"
#include <AnimatedSpriteGameObject.h>
#include <Input.h>

class Player : public yam2d::AnimatedSpriteGameObject
{
public:
	Player(int gameObjectType, yam2d::SpriteSheet* sprite, HitCheck* HitChecker, yam2d::TmxMap* Map);
	
	void SetPathfinder(Pathfinding* pathfinder);
	
	virtual ~Player();
	virtual void update( float deltaTime );
	void modifyWeight(double Calories);
	bool setZoom(float newZoom);


private:
	yam2d::vec2 movement(float deltaTime);
	void objectCheck();
	void calculateWeight(yam2d::vec2 Movement, float DeltaTime);
	void setupAnimations();

	
	std::vector<vec2> path;
	double calories;

	int dirAnimation;

	bool clearDestination;

	bool MOUSE_LEFT_released;
	yam2d::vec2 direction;
	yam2d::vec2 destination;
	HitCheck* hitboxCheck;
	Pathfinding* pathfinder;
	yam2d::TmxMap* map;
	float zoom;
	bool end;
};

#endif