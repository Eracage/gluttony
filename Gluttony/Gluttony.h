#ifndef GLUTTONY_H
#define GLUTTONY_H

#include <es_util.h>
#include <Map.h>
#include <Layer.h>
#include <Tile.h>
#include <Camera.h>
#include "Player.h"
#include "HitCheck.h"
#include "Pathfinding.h"
#include "FoodControl.h"


using namespace yam2d;

//class Player;

class Gluttony
{
public:
	Gluttony(ESContext *esContext);
	~Gluttony();
	void Update(ESContext *esContext, float DeltaTime);
	void Draw(ESContext *esContext);
private:
	vec2 zoomControl(ESContext *esContext, float DeltaTime);

	TmxMap* map;
	HitCheck* hitChecker;
	Pathfinding* pathfinder;
	FoodControl* foodControl;

	Player* player;

	float zoom;

	static const int number = 0;
	Player* Enemies[number];
};

#endif GLUTTONY_H
