#ifndef FOODCONTROL_H
#define FOODCONTROL_H

#include "Food.h"
#include "HitCheck.h"
#include "Player.h"

struct FoodSpawn
{
	float delay;
	vec2 position;
	float SizeX;
	float SizeY;
	std::string spawnName;
	std::vector<Food> availableFoods;
};

class FoodControl
{
public:
	FoodControl(Player* player, HitCheck* hitCheck, TmxMap* map);
	~FoodControl();
	
	void update(float deltaTime);

	void addFood(Food* food, vec2 position);

private:
	
	void addFood(FoodSpawn* Spawn);

	void makeFoods();
	
	void defineSpawnerFoods();

	float timer;

	Player* player;
	HitCheck* hitCheck;
	TmxMap* map;

	Food* burger;

	std::vector<Meal*> foods;
	std::vector<FoodSpawn*> foodSpawns;
};

#endif