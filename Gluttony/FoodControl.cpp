#include "FoodControl.h"

#define PI 3.14159265

FoodControl::FoodControl(Player* player, HitCheck* hitCheck, TmxMap* map)
	: player(player),
	  hitCheck(hitCheck),
	  map(map),
	  timer(0)
{
	Layer::GameObjectList spawns = map->getLayer("FoodSpawns")->getGameObjects();
	for (unsigned int i = 0; i < spawns.size(); ++i)
	{
		FoodSpawn* spawn = new FoodSpawn();
		spawn->delay = float(rand()%6 + 10) / (rand()%6 +1);
		spawn->position = spawns[i]->getPosition() - vec2(0.5f);
		spawn->SizeX = spawns[i]->getSize().x;
		spawn->SizeY = spawns[i]->getSize().y;
		spawn->spawnName = spawns[i]->getName();
		foodSpawns.push_back(spawn);
	}

	makeFoods();

	defineSpawnerFoods();
}

FoodControl::~FoodControl()
{

	for (int i = 0; i < foods.size(); ++i)
	{
		map->deleteGameObject(foods[i]);
		foods.erase(foods.begin() + i);
	}

	for (int i = 0; i < foodSpawns.size(); ++i)
	{
		delete foodSpawns[i];
	}

	delete burger;
}

void FoodControl::update(float deltaTime)
{
	timer += deltaTime;

	for (size_t i = 0; i < foodSpawns.size(); ++i)
	{
		if (timer > foodSpawns[i]->delay)
		{
			foodSpawns[i]->delay += float(rand()%18 + 4) / (rand()%6 +1);

			addFood(foodSpawns[i]);
		

			//for (size_t j = foods.size() -1; j > 0 ; --j)
			//{
			//	if (rand()%10 < 1)
			//	{
			//		addFood(foods[j]->food,foods[j]->getPosition());
			//		foods[j]->direction += PI/2.0f;
			//	}
			//}
		}
	}

	


	vec2 playerpos = player->getPosition();


	for (size_t i = foods.size(); i > 0; --i)
	{
		int j = i - 1;
		Meal* food = foods[i - 1];
		vec2 foodpos = food->getPosition();

		// Check if player gets near enough to eat food

		if ((playerpos - foodpos).Length() < (player->getSize().x + food->getSize().x)/2.0f/map->getTileWidth() )
		{
			player->modifyWeight(food->getCalories());
			map->deleteGameObject(food);
			foods.erase(foods.begin() + j);
		}

		// Move code

		vec2 movement = vec2(
			cos(food->direction) * deltaTime,
			sin(food->direction) * deltaTime
			);
		food->setPosition(food->getPosition() + movement);

		vec2 hit = hitCheck->CheckMapHit(food, 8);
		if (hit == food->getPosition());
		else
		{
			float dirDelta = (rand()%1000)/2000.0f;
			if (hit.x == foodpos.x) // Y suunnan t�rm�ys
				food->direction = -food->direction - dirDelta;
			else if (hit.x == foodpos.x) // X suunnan t�rm�ys
				food->direction = -(PI/2.0f + food->direction) - PI/2.0f - dirDelta;
			else
				food->direction += PI/2.0f - dirDelta;

			food->setPosition(hit);
		}


	}
}

void FoodControl::addFood(Food* food, vec2 position)
{
	foods.push_back(new Meal(food, position));
	map->getLayer("GameObjects")->addGameObject(foods.back());
}

void FoodControl::addFood(FoodSpawn* Spawn)
{
	if (Spawn->availableFoods.size() > 0)
	{
		Food* food = &Spawn->availableFoods[rand()%Spawn->availableFoods.size()];
		vec2 position = Spawn->position + 
			vec2(
			( (rand() %1000) /1000.0f - 0.5f) * (Spawn->SizeX/2.0f),
			( (rand() %1000) /1000.0f - 0.5f) * (Spawn->SizeX/2.0f)
			);
		addFood(food,position);
	}
}

void FoodControl::makeFoods()
{
	burger = new Food();
	burger->name = "burger";
	burger->calories = 520.0f;
	burger->sprite = SpriteSheet::generateSpriteSheet(new Texture("burger.png"),64,64,0,0);
	foods.push_back(new Meal(burger,vec2(-1000)));
}

void FoodControl::defineSpawnerFoods()
{
	for (int i = 0; i < foodSpawns.size(); i++)
	{
		foodSpawns[i]->availableFoods.push_back(*burger);
	}
}