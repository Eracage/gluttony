#include "Gluttony.h"
#include <time.h>
#include <iostream>
Layer* createNewLayer(void* userData, Map* map, const std::string& name, float opacity, bool visible, const PropertySet& properties)
{		
	esLogMessage("Creating static layer!");

	// Check that if "static"-propery is set to Leyer properties, and if it's value is "true" or 1
	if( properties.hasProperty("static") && 
		(properties.getLiteralProperty("static")=="true" || properties.getLiteralProperty("static")=="1")  )
	{
		// Is, so, then create static layer, which is batched only once, at first call to Map::render
		return new Layer(map, name, opacity, visible, true, properties);
	}

	// by default create dynamic layer, which is batced each frame
	return new Layer(map, name, opacity, visible, false, properties); 
}

Gluttony::Gluttony(ESContext *esContext)
	: zoom(1.0f)
{
	srand(time(NULL));

	map = new TmxMap();
	map->registerCreateNewLayerFunc(createNewLayer);
	map->loadMapFile("tilemap.tmx");


	hitChecker = new HitCheck(map);
	pathfinder = new Pathfinding(map);

	Texture* playerTexture = new Texture("FatGuy2.png");
	SpriteSheet* playerSprite = SpriteSheet::generateSpriteSheet(playerTexture,192,192,0,0);

	player = new Player(0,playerSprite,hitChecker, map);
	player->setName("Player");
	player->SetPathfinder(pathfinder);
	foodControl = new FoodControl(player,hitChecker,map);

	//

	map->getLayer("GameObjects")->addGameObject(player);
	map->getCamera()->setPosition( vec2(map->getWidth()/2.0f-0.5f, map->getHeight()/2.0f-0.5f));
	map->getCamera()->setScreenSize(esContext->width,esContext->height,esContext->height/zoom);
	
	// Animations
	
	for (int i = 0; i < number; i++)
	{
		Enemies[i] = new Player(0,playerSprite,hitChecker, map);
		Enemies[i]->setName("Player" + i);
		Enemies[i]->SetPathfinder(pathfinder);
		map->getLayer("GameObjects")->addGameObject(Enemies[i]);
	}


}


Gluttony::~Gluttony()
{
	delete foodControl;
	delete map;
	delete hitChecker;
	delete pathfinder;
}

void Gluttony::Update(ESContext* esContext, float DeltaTime)
{

	map->update(DeltaTime);
	foodControl->update(DeltaTime);

	vec2 CameraPos = zoomControl(esContext, DeltaTime);
	map->getCamera()->setPosition(CameraPos);
}

void Gluttony::Draw(ESContext *esContext)
{
	map->render();
}

vec2 Gluttony::zoomControl(ESContext *esContext, float DeltaTime)
{
	float mapWidth = map->getWidth();
	float mapHeight = map->getHeight();

	vec2 CameraPos = vec2(player->getPosition().x, player->getPosition().y) + vec2(0,4/32.0f);
	vec2 TL = vec2(0.5f);
	vec2 RB = vec2(mapWidth, mapHeight)  - vec2(0.5f);

	float cameraWidthInMap = ((float) esContext->width) / map->getTileWidth() / zoom / 2.0f;
	float cameraHeightInMap = ((float) esContext->height) / map->getTileHeight() / zoom / 2.0f;
	
	if ((RB-CameraPos).x <= cameraWidthInMap)
		CameraPos.x = mapWidth - 0.5f - cameraWidthInMap;

	if ((TL+CameraPos).x <= cameraWidthInMap)
	{
		CameraPos.x = - 0.5f + cameraWidthInMap;

		if ((RB-CameraPos).x <= cameraWidthInMap)
		{
			zoom = esContext->width / map->getTileWidth()/ ((RB-CameraPos).x)/ 2;
			CameraPos.x = mapWidth - 0.5f - cameraWidthInMap;
		}
	}
	
	if ((RB-CameraPos).y <= cameraHeightInMap)
		CameraPos.y = mapHeight - 0.5f - cameraHeightInMap;

	if ((TL+CameraPos).y <= cameraHeightInMap)
	{
		CameraPos.y = - 0.5f + cameraHeightInMap;

		if ((RB-CameraPos).y <= cameraHeightInMap)
		{
			zoom = esContext->width / map->getTileWidth()/ ((RB-CameraPos).x)/ 2;
			CameraPos.y = mapHeight - 0.5f - cameraHeightInMap;
		}
	}

	zoom += (getKeyState(KEY_ADD) - getKeyState(KEY_SUBTRACT))*zoom*DeltaTime;

	
	float minzoom = 0.5f;
	if (zoom < minzoom) zoom = minzoom;

	
	map->getCamera()->setScreenSize(esContext->width,esContext->height,esContext->height/zoom);

	if(player->setZoom(zoom))
	{
		zoom = 20;
	}

	return CameraPos;
}