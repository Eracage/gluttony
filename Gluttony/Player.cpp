#include "Player.h"
#include <iostream>

using namespace yam2d;

Player::Player(int gameObjectType, SpriteSheet* sprite, HitCheck* HitChecker, TmxMap* Map)
	: AnimatedSpriteGameObject(gameObjectType,sprite),
	  hitboxCheck(HitChecker),
	  map(Map),
	  calories(5000.0f),
	  dirAnimation(0),
	  clearDestination(false),
	  zoom(1.0f)
{
	setSize(16,16);
	setPosition(vec2(15,20));
	destination = getPosition();
	end = false;

	setupAnimations();
}

void Player::SetPathfinder(Pathfinding* Pathfinder)
{
	pathfinder = Pathfinder;
}

Player::~Player()
{
}

void Player::update( float deltaTime)
{
	AnimatedSpriteGameObject::update(deltaTime);

	vec2 playerMovement = movement(deltaTime);

	calculateWeight(playerMovement, deltaTime);

	objectCheck();
}

void Player::modifyWeight(double Calories)
{
	calories += Calories;
}

bool Player::setZoom(float newZoom)
{
	zoom = newZoom;
	return end;
}

vec2 Player::movement(float deltaTime)
{
	float moveSpeed = 10.0f - getSize().x/8;

	float horizontal = float(getKeyState(KEY_RIGHT)-getKeyState(KEY_LEFT));
	float vertical = float(getKeyState(KEY_DOWN)-getKeyState(KEY_UP));

	if (!getMouseButtonState(MOUSE_LEFT))
		MOUSE_LEFT_released = true;
	else if (MOUSE_LEFT_released)
	{
		MOUSE_LEFT_released = false;
		vec2 mousepos = map->screenToMapCoordinates(vec2(getMouseAxisX()/zoom, getMouseAxisY()/zoom ));
		if(pathfinder->FindPathYam2D(getPosition().x, getPosition().y, mousepos.x, mousepos.y,getSizeInTiles().x))
		{
			path.clear();
			path = pathfinder->GetPath();

			destination = path[0];
			if (path.size() > 1)
				*path.erase(path.begin());
		}
	}
	
	if(horizontal || vertical)
		direction = vec2(horizontal,vertical);
	else
		direction = (destination - getPosition());

	direction.Normalize();

	vec2 playerMovement = deltaTime*moveSpeed*direction;

	if(horizontal || vertical)
	{
		clearDestination = true;
		path.clear();
	}
	else if (playerMovement.Length() > (destination - getPosition()).Length())
	{
		playerMovement = destination - getPosition();
		
		if (path.size() > 0)
			destination = path[0];
		if (path.size() > 1)
			*path.erase(path.begin());
	}

	//animations
	if (calories > 15000)
	{
		setActiveAnimation(5);
		end = true;
	}
	else if (calories < 2000)
	{
		setActiveAnimation(6);
		end = true;
	}
	else if (playerMovement == vec2(0))
	{
		setActiveAnimation(dirAnimation);
	}
	else
	{
		if (playerMovement.y > abs(playerMovement.x) ) // moving down
			dirAnimation = 0;
		if (playerMovement.y < -abs(playerMovement.x) ) // moving up
			dirAnimation = 10;
		if (abs(playerMovement.y) <= playerMovement.x) // moving right
			dirAnimation = 20;
		if (-abs(playerMovement.y) >= playerMovement.x) // moving right
			dirAnimation = 30;
		if (getActiveAnimation() != dirAnimation +1)
			setActiveAnimation(dirAnimation + 1);
	}


	setPosition(getPosition() + playerMovement);
	float dif = 0;
	if (int(getSize().x) % 16 < 2)
		dif = int(getSize().x) % 16;

	if (path.size() < 2)
	{
		vec2 hit = hitboxCheck->CheckMapHit(this, dif*2);

		if(hit.Length() > 0)
		{
			setPosition(hit);
		}
	}




	if (clearDestination)
	{
		clearDestination = false;
		destination = getPosition();
	}
	
	return playerMovement;
}

void Player::objectCheck()
{
	if (hitboxCheck->GetNameOfObject(this) == ("road1"))
	{
		std::cout<<"road1";
	}
	if (hitboxCheck->GetNameOfObject(this) == ("road2"))
	{
		std::cout<<"road2";
	}
	if (hitboxCheck->GetNameOfObject(this) == ("road3"))
	{
		std::cout<<"road3";
	}
}

void Player::calculateWeight(vec2 Movement, float DeltaTime)
{
	setSize(vec2(sqrt(calories) / 4) );
	calories -= Movement.Length() * 3.0f;
	calories -= DeltaTime * 2.4f;
}

void Player::setupAnimations()
{
	std::vector<int> indices;
	indices.resize(1);
	
	for( int i=0; i<4; ++i )
	{
		for( size_t j=0; j<indices.size(); ++j) {indices[j] = 8*i + j;}
		addAnimation(i*10, SpriteAnimation::SpriteAnimationClip(indices,2, 1.0f, true));
	}
	indices[0] = 5;
	addAnimation(5, SpriteAnimation::SpriteAnimationClip(indices,2, 1.0f, true));
	indices[0] = 6;
	addAnimation(6, SpriteAnimation::SpriteAnimationClip(indices,2, 1.0f, true));
	indices.resize(4);
	for( int i=0; i<4; ++i )
	{
		for( size_t j=0; j<indices.size(); ++j) {indices[j] = 8*i + j + 1;}
		addAnimation(i*10+1, SpriteAnimation::SpriteAnimationClip(indices,2, 1.0f, true));
	}
	
	setActiveAnimation(5);
}